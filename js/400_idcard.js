var currentHash = ""
var IDCardFile = ""
var IDCardImage = ""

function OnStart()
{
  var name = window.localStorage.getItem('myName')
  if (name.length>0) {
    document.getElementById("btn_name").innerHTML = "<span class='icon-user'></span>" + name
    document.getElementById("btn_name").classList.add("done_button");
  }
  
  var birthday = window.localStorage.getItem('myBirthday')
  if (birthday.length == 10) {
    document.getElementById("btn_birthday").innerHTML = "<span class='icon-calendar'></span>" + birthday
    document.getElementById("btn_birthday").classList.add("done_button");
  }

  var gender = window.localStorage.getItem('myGender')
  if (gender.length > 0) {
    document.getElementById("btn_gender").innerHTML = "<span class='icon-man-woman'></span>" + gender
    document.getElementById("btn_gender").classList.add("done_button");
  }

  var long = window.localStorage.getItem('myLength')
  if (long.length > 0) {
    document.getElementById("btn_length").innerHTML = "<span class='icon-shift'></span>" + long
    document.getElementById("btn_length").classList.add("done_button");
  }

  var hair = window.localStorage.getItem('myHair')
  if (hair.length > 0) {
    document.getElementById("btn_hair").innerHTML = "<span class='icon-cloud'></span>" + hair
    document.getElementById("btn_hair").classList.add("done_button");
  }

  var lefteye = window.localStorage.getItem('myLeftEye')
  if (lefteye.length > 0) {
    document.getElementById("btn_lefteye").innerHTML = "<span class='icon-eye'></span>" + lefteye
    document.getElementById("btn_lefteye").classList.add("done_button");
  }

  var righteye = window.localStorage.getItem('myRightEye')
  if (righteye && righteye.length > 0) {
    document.getElementById("btn_righteye").innerHTML = "<span class='icon-eye'></span>&nbsp;"+righteye
    document.getElementById("btn_righteye").classList.add("done_button");
  }


  var features = window.localStorage.getItem('myFeatures')
  if (features !== null) {
    var ss = features;
    //alert(ss)
    if (ss == "") {ss = "[None]"}
    document.getElementById("btn_features").innerHTML = "<span class='icon-hipster'></span>" + ss
    document.getElementById("btn_features").classList.add("done_button");
  }


  var s1 = window.localStorage.getItem('myPrevidcards')
  if (s1 != null) {
    var ss = s1 +" Previous IDCards"
    document.getElementById("btn_prevcards").innerHTML = "<span class='icon-drawer'></span>" + ss
    document.getElementById("btn_prevcards").classList.add("done_button");
  }
  ////////////////////////////////////////
  ///  See if the IDCard can be shown  ///
  ////////////////////////////////////////
  currentHash = ""
  var s1 = window.localStorage.getItem('myCurrentHash')
  if (s1 == null) {s1 = ""}
  if ((s1 != s1) || (s1 == "")) //catch a NaN issue
  {
    currentHash = ""
    document.getElementById("IDCard2").src = "img/PhotoClear.png"
  }
  else
  {
    ////////////////////////////////////////
    ///  There must be a IDCard present  ///
    ////////////////////////////////////////
    currentHash = s1
    var s1 = window.localStorage.getItem('myFiles')
    if (s1 == null) {s1 = ""}
    if (s1 != s1) //catch a NaN issue
    {
      alert("ERROR: There is no IDCard found, this is strange because the hashcode of your IDCard is found, which should not be possible.")
    }
    else
    {
      IDCardFile = s1+".png"
      var IDCardImage = window.localStorage.getItem(IDCardFile)
      if (IDCardImage == null) {IDCardImage = ""}
      if (IDCardImage != IDCardImage) //catch a NaN issue
      {
        alert("ERROR: There is no IDCard found, this is strange because the filename of your IDCard is found, which should not be possible.")
      }
      else
      {
        document.getElementById("IDCard2").src = IDCardImage; //is the Base64 code
      }
    }
  }
}


function IDCardClick()
{
  ///////////////////////////////////////////////////
  ///  First test if all the buttons are clicked  ///
  ///////////////////////////////////////////////////
  var err = false
  var name = window.localStorage.getItem('myName')
  err = (err || (name == null))
  var birthday = window.localStorage.getItem('myBirthday')
  err = (err || (birthday == null))
  var gender = window.localStorage.getItem('myGender')
  err = (err || (gender == null))
  var long = window.localStorage.getItem('myLength')
  err = (err || (long == null))
  var hair = window.localStorage.getItem('myHair')
  err = (err || (hair == null))
  var lefteye = window.localStorage.getItem('myLeftEye')
  err = (err || (lefteye == null))
  var righteye = window.localStorage.getItem('myRightEye')
  err = (err || (righteye == null))
  var features = window.localStorage.getItem('myFeatures')
  err = (err || (features == null))
  var prevcards = window.localStorage.getItem('myPrevidcards')
  err = (err || (prevcards == null))
  if (err)
  {
    alert("Please first fill out all the info under the buttons above.")
  }
  else
  {
    window.location.href = "410_picture.html";
  }

}



// function fileTest()
// {

//  //"/home/teun/Documents/1CoinH/Browser/mysdfile.txt");
//   var fileName = "mysdfile.txt";
//   var fileContent = "Page content...";
//   var myFile = new Blob([fileContent], {type: 'text/plain'});

//   window.URL = window.URL || window.webkitURL;
//   var dlBtn = document.getElementById("download");

//   dlBtn.setAttribute("href", window.URL.createObjectURL(myFile));
//   dlBtn.setAttribute("download", fileName);
// }


// /**
// *  Simple JavaScript Promise that reads a file as text.
// **/
// function readFileAsText(file)
// {
//   return new Promise(function(resolve,reject){
//       let fr = new FileReader();

//       fr.onload = function(){
//           resolve(fr.result);
//       };

//       fr.onerror = function(){
//           reject(fr);
//       };

//       fr.readAsText(file);
//   });
// }


// // Handle fileupload
// document.getElementById("fileinput").addEventListener("change", function(ev)
// {
//   let file = ev.currentTarget.files[0];

//   // Abort file reading if no file was selected
//   if(!file) return;

//   readFileAsText(file).then((fileContent) => {
//       // Print file content on the console
//       console.log(fileContent);
//   });
// }, false);