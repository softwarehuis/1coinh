
/////////////////////////
///  Set global vars  ///
/////////////////////////
var canvas = document.getElementsByTagName('canvas');
var ctx = canvas[0].getContext("2d");
var ctx2 = canvas[1].getContext("2d");
var edge = 5 // the size in percentage for the passpartoute edge
var touchX, touchY, otx, oty, ttx, tty;
console.log("Rendering canvas")
var br = 100.0;
var co = 100.0;
var sa = 100.0;
var ran0 = 50;
var ran1 = 50;
var ran2 = 50;
var ran3 = 50;
var sliderselected = 1;
var img = new Image;
var canvasWidth = canvas[0].width;
var canvasHeight = canvas[0].width;
console.log(canvasWidth, canvasHeight);
var imageWidth = 0;
var imageHeight = 0;
var rot = 0;
var focx = 0; //this is the x focuspoint where the image is centered in the border the left top of the image is 0
var focy = 0; //this is the y focuspoint where the image is centered in the border the left top of the image is 0
var ox = 0;
var oy = 0;
var imw = 0;
var imh = 0;
var image1 = new Image;
var imb1 = false;
var preload = true;
var mx1 = 0;
var my1 = 0;
var mx2 = 0;
var my2 = 0;
var isDragging = false;
var portx1 = 0;
var porty1 = 0;
var portx2 = 0;
var porty2 = 0;
var scale = 100;
var scale2 = 100;
var picsize = 1;
var touchX, touchY;

const load_button = document.getElementById("loadpicture"); //is the invisible button
const rotate_button = document.getElementById("rotate");
const reset_button = document.getElementById("reset");
const back_button = document.getElementById("back");
const logo_button = document.getElementById("logo-d");
const next_button = document.getElementById("next");
const slide_button = document.getElementById("myRange");
const lightIcons = document.getElementById("lightIcons");
const enlarge_button = document.getElementById("enlarge");
const contrast_button = document.getElementById("contrast");
const brightness_button = document.getElementById("brightness");
const colour_button = document.getElementById("colour");
const icon1_button = document.getElementById("icon1");
const icon2_button = document.getElementById("icon2");
const icon3_button = document.getElementById("icon3");
const icon4_button = document.getElementById("icon4");
const generate_button = document.getElementById("IDCardGenerate");


function redraw(preload, edge, image) {
  function _border(preload, edge) {
    if (!preload) {
      // Set the border at 10% of the width
      var margin = (edge / 100) * canvas[0].width;
      var width = canvas[0].width;
      var height = canvas[0].height;
      ctx.fillStyle = "rgba(5, 5, 5, .8)";
      // horizontal bars
      ctx.fillRect(0, 0, width, margin); // top
      ctx.fillRect(0, height - margin, width, margin); // bottom
      // vertical bars
      ctx.fillRect(0, margin, margin * 2, height - (margin * 2)); // left
      ctx.fillRect(width - (margin * 2), margin, margin * 2, height - (margin * 2)); // right
      
  
    }
  }

  function _image(image) {
    if (image) {
      
      var ctx = canvas[0].getContext("2d");
      if (ctx) {
        var canvas_width = canvas[0].width;
        var canvas_height = canvas[0].height;
        var image_width = image.naturalWidth;
        var image_height = image.naturalHeight;

        var hRatio = canvas_width / image_width;
        var vRatio = canvas_height / image_height;
        ctx.drawImage(image, 0, 0,
          image_width, image_height, 0,0, 
          image_width*hRatio, image_height*vRatio);

      }
    }
  }
  
  function _backdrop(preload) {
    if (!preload) {
      for (let i = 0; i < canvas.length; i++) {
        ctx.clearRect(0, 0, canvas[i].width, canvas[i].height);
        ctx.strokeStyle = "rgb(255, 0, 0)";
        ctx.fillStyle = "white";
        ctx.fillRect(0, 0, canvas[i].width, canvas[i].height);
      }
    }
  }
  _backdrop(preload);
  _image(image);
  _border(preload, edge);
}


/**
 * This procedure is called after custom-file-upload button
 *
 * @param {*} e the Event to listen to
 */
function handleFile(e) {
  if (e.target.files[0] == null) {
    alert("No file selected!");
  } else {
    img.onload = function () {
      console.log(img)
      redraw(preload, edge, img)
      ran0 = 50;
      ran1 = 50;
      ran2 = 50;
      ran3 = 50;
      slide_button.value = ran0;
      document.getElementById("slider").hidden = false
      enlarge_button.hidden = false
      contrast_button.hidden = false
      brightness_button.hidden = false
      colour_button.hidden = false
      rotate_button.hidden = false
      reset_button.hidden = false
      icon1_button.classList.remove(icon1_button.classList.item(0))
      icon1_button.classList.add("image_button");
      icon2_button.classList.remove(icon2_button.classList.item(0))
      icon2_button.classList.add("imageCold_button");
      icon3_button.classList.remove(icon3_button.classList.item(0))
      icon3_button.classList.add("imageCold_button");
      icon4_button.classList.remove(icon4_button.classList.item(0))
      icon4_button.classList.add("imageCold_button");
      rotate_button.classList.remove(rotate_button.classList.item(0))
      rotate_button.disabled = false;
      reset_button.disabled = false;
      generate_button.classList.remove("disabled_button")
      generate_button.disabled = false;
    }
    img.onerror = function () {
      alert("Error occurred while loading image");
    }
  }


  if (!(e.target.files[0] == null)) {
    img.src = URL.createObjectURL(e.target.files[0]);
  }
}


function doMouseDown(e) {
  mx1 = e.clientX;
  my1 = e.clientY;
  isDragging = true;
}


function doMouseMove(e) {
  //////////////////////////////////////////////
  ///  only place wher the focus may change  ///
  //////////////////////////////////////////////
  mx2 = e.clientX;
  my2 = e.clientY;
  if (isDragging) {
    switch (rot) {
      case 0: {
        focx = focx + (mx1 - mx2); mx1 = mx2;
        focy = focy + (my1 - my2); my1 = my2;
        focx0 = focx;
        focy0 = focy;
      }; break;
      case 1: {
        focx = focx + (my1 - my2); my1 = my2;
        focy = focy + (mx2 - mx1); mx1 = mx2;
        focx0 = focx;
        focy0 = focy;
      }; break;
      case 2: {
        focx = focx - (mx1 - mx2); mx1 = mx2;
        focy = focy - (my1 - my2); my1 = my2;
        focx0 = focx;
        focy0 = focy;
      }; break;
      case 3: {
        focx = focx - (my1 - my2); my1 = my2;
        focy = focy - (mx2 - mx1); mx1 = mx2;
        focx0 = focx;
        focy0 = focy;
      }; break;
    };
    co = Math.round(100 * Math.pow(1.02, (50 - ran1)));
    br = Math.round(100 * Math.pow(1.02, (50 - ran2)));
    sa = Math.round(100 * Math.pow(1.02, (50 - ran3)));
    ctx2.filter = "contrast(" + co.toString() + "%) brightness(" + br.toString() + "%) saturate(" + sa.toString() + "%)";
    drawBackdrop();
    drawImg();
    drawBorder(10);
  };
}


function doMouseUp(e) {
  isDragging = false;
}


function doMouseOut(e) {
  isDragging = false;
}


function sketchpad_touchStart() {
  getTouchPos();
  ttx = touchX;
  tty = touchY;
  // Prevents an additional mousedown event being triggered
  event.preventDefault();
}


function sketchpad_touchMove(e) {
  getTouchPos(e);
  /////////////////////////////////////////////////////////////////////////////////////////////////
  ///  During a touchmove event, unlike a mousemove event, we don't need to check if            ///
  ///  the touch is engaged, since there will always be contact with the screen by definition.  ///
  /////////////////////////////////////////////////////////////////////////////////////////////////
  otx = touchX;
  oty = touchY;
  switch (rot) {
    case 0: {
      focx = focx + (ttx - otx); ttx = otx;
      focy = focy + (tty - oty); tty = oty;
      focx0 = focx;
      focy0 = focy;
    }; break;
    case 1: {
      focx = focx + (tty - oty); tty = oty;
      focy = focy + (otx - ttx); ttx = otx;
      focx0 = focx;
      focy0 = focy;
    }; break;
    case 2: {
      focx = focx - (ttx - otx); ttx = otx;
      focy = focy - (tty - oty); tty = oty;
      focx0 = focx;
      focy0 = focy;
    }; break;
    case 3: {
      focx = focx - (tty - oty); tty = oty;
      focy = focy - (otx - ttx); ttx = otx;
      focx0 = focx;
      focy0 = focy;
    }; break;
  };
  co = Math.round(100 * Math.pow(1.02, (50 - ran1)));
  br = Math.round(100 * Math.pow(1.02, (50 - ran2)));
  sa = Math.round(100 * Math.pow(1.02, (50 - ran3)));
  ctx2.filter = "contrast(" + co.toString() + "%) brightness(" + br.toString() + "%) saturate(" + sa.toString() + "%)";
  drawBackdrop();
  drawImg();
  drawBorder(10);
  //////////////////////////////////////////////////////////////////////////////
  ///  Prevent a scrolling action as a result of this touchmove triggering.  ///
  //////////////////////////////////////////////////////////////////////////////
  event.preventDefault();
}


function getTouchPos(e) {
  if (!e) {
    var e = event;
  }
  if (e.touches) {
    if (e.touches.length == 1) { // Only deal with one finger
      var touch = e.touches[0]; // Get the information for finger #1
      touchX = touch.pageX - touch.target.offsetLeft;
      touchY = touch.pageY - touch.target.offsetTop;
    }
  }
}


function rotate() {
  ctx.clearRect(0, 0, canvasWidth, canvasHeight);
  ctx2.clearRect(0, 0, canvasWidth, canvasHeight);
  rot = rot + 1;
  if (rot == 4) { rot = 0 };
  var focxx = focx;
  co = Math.round(100 * Math.pow(1.02, (50 - ran1)));
  br = Math.round(100 * Math.pow(1.02, (50 - ran2)));
  sa = Math.round(100 * Math.pow(1.02, (50 - ran3)));
  ctx2.filter = "contrast(" + co.toString() + "%) brightness(" + br.toString() + "%) saturate(" + sa.toString() + "%)";
  drawBackdrop();
  drawImg();
  drawBorder(10);
}


function resetslides() {
  document.getElementById("myRange").value = 50;
  ran0 = 50;
  ran1 = 50;
  ran2 = 50;
  ran3 = 50;
  co = Math.round(100 * Math.pow(1.02, (50 - ran1)));
  br = Math.round(100 * Math.pow(1.02, (50 - ran2)));
  sa = Math.round(100 * Math.pow(1.02, (50 - ran3)));
  ctx2.filter = "contrast(" + co.toString() + "%) brightness(" + br.toString() + "%) saturate(" + sa.toString() + "%)";
  scale = 100;
  scale2 = 100;
  drawBackdrop();
  drawImg();
  drawBorder(10);
}


function myFunction(e) {
  /////////////////////////////////////////////////////
  ///  Check whether the wheel event is supported.  ///
  /////////////////////////////////////////////////////
  if (e.type == "wheel") supportsWheel = true;
  else if (supportsWheel) return;
  ///////////////////////////////////////////////////////////////////////
  ///  Determine the direction of the scroll (< 0 ? up, > 0 ? down).  ///
  ///////////////////////////////////////////////////////////////////////
  if (sliderselected != 1) {
    enlargeIt()
    posit = document.getElementById("myRange").value;
  }
  else {
    var delta = 0;
    delta = ((e.deltaY || -e.wheelDelta || e.detail) >> 10) || 1;
    var posit = 0;
    posit = document.getElementById("myRange").value;
    total = Number(posit) + Number(delta);
    if (total < 0) { total = 0; };
    if (total > 100) { total = 100 };
    scale2 = scale;
    scale = (100 * Math.pow(1.04, (50 - total))); //107?
    if (!(scale2 == 0)) { scale2 = (scale / scale2) };
    co = Math.round(100 * Math.pow(1.02, (50 - ran1)));
    br = Math.round(100 * Math.pow(1.02, (50 - ran2)));
    sa = Math.round(100 * Math.pow(1.02, (50 - ran3)));
    ctx2.filter = "contrast(" + co.toString() + "%) brightness(" + br.toString() + "%) saturate(" + sa.toString() + "%)";
    drawBackdrop();
    drawImg();
    drawBorder(10);
    document.getElementById("myRange").value = total;
    ran0 = total;
  }
}


function drawOldImage() {
  if (imb1) {
    if (!image1.complete) {
      setTimeout(function () { drawOldImage(); }, 50); return;
    };
    ctx.drawImage(image1, portx1, porty1, 256, 256);
    ctx2.drawImage(image1, portx1, porty1, 256, 256);
    ctx.font = "22px Arial";
    ctx.fillStyle = "black";
    var ll = Math.round(ctx.measureText("LOAD NEW IMAGE?").width / 2);
    ctx.fillText("LOAD NEW IMAGE?", Math.round(canvas[0].width / 2) - ll, porty2 + 30);
    generate_button.classList.remove(generate_button.classList.item(0))
    generate_button.disabled = false;
    generate_button.classList.add("main_button");
  }
}


function enlargeIt() {
  icon1_button.classList.remove(icon1_button.classList.item(0))
  icon1_button.classList.add("image_button");
  icon2_button.classList.remove(icon2_button.classList.item(0))
  icon2_button.classList.add("imageCold_button");
  icon3_button.classList.remove(icon3_button.classList.item(0))
  icon3_button.classList.add("imageCold_button");
  icon4_button.classList.remove(icon4_button.classList.item(0))
  icon4_button.classList.add("imageCold_button");
  sliderselected = 1;
  slide_button.value = ran0;
}


function contrastIt() {
  icon1_button.classList.remove(icon1_button.classList.item(0))
  icon1_button.classList.add("imageCold_button");
  icon2_button.classList.remove(icon2_button.classList.item(0))
  icon2_button.classList.add("image_button");
  icon3_button.classList.remove(icon3_button.classList.item(0))
  icon3_button.classList.add("imageCold_button");
  icon4_button.classList.remove(icon4_button.classList.item(0))
  icon4_button.classList.add("imageCold_button");
  sliderselected = 2;
  slide_button.value = ran1;
}


function brightnessIt() {
  icon1_button.classList.remove(icon1_button.classList.item(0))
  icon1_button.classList.add("imageCold_button");
  icon2_button.classList.remove(icon2_button.classList.item(0))
  icon2_button.classList.add("imageCold_button");
  icon3_button.classList.remove(icon3_button.classList.item(0))
  icon3_button.classList.add("image_button");
  icon4_button.classList.remove(icon4_button.classList.item(0))
  icon4_button.classList.add("imageCold_button");
  sliderselected = 3;
  slide_button.value = ran2;
}


function colourIt() {
  icon1_button.classList.remove(icon1_button.classList.item(0))
  icon1_button.classList.add("imageCold_button");
  icon2_button.classList.remove(icon2_button.classList.item(0))
  icon2_button.classList.add("imageCold_button");
  icon3_button.classList.remove(icon3_button.classList.item(0))
  icon3_button.classList.add("imageCold_button");
  icon4_button.classList.remove(icon4_button.classList.item(0))
  icon4_button.classList.add("image_button");
  sliderselected = 4;
  slide_button.value = ran3;
}


function enlageGo() {
  /////////////////////////////////
  ///  ENLARGE ONLY ON CANVAS2  ///
  /////////////////////////////////
  ran0 = slide_button.value
  var posit = slide_button.value;
  scale2 = scale;
  scale = (100 * Math.pow(1.04, (50 - posit))); //1.07?
  if (!(scale2 == 0)) { scale2 = (scale / scale2); };
  co = Math.round(100 * Math.pow(1.02, (50 - ran1)));
  br = Math.round(100 * Math.pow(1.02, (50 - ran2)));
  sa = Math.round(100 * Math.pow(1.02, (50 - ran3)));
  ctx2.filter = "contrast(" + co.toString() + "%) brightness(" + br.toString() + "%) saturate(" + sa.toString() + "%)";
  drawBackdrop();
  drawImg();
  drawBorder(10);
}


function contrastGo() {
  //////////////////////////////////
  ///  CONTRAST ONLY ON CANVAS2  ///
  //////////////////////////////////
  ran1 = slide_button.value
  co = Math.round(100 * Math.pow(1.02, (50 - ran1)));
  br = Math.round(100 * Math.pow(1.02, (50 - ran2)));
  sa = Math.round(100 * Math.pow(1.02, (50 - ran3)));
  ctx2.filter = "contrast(" + co.toString() + "%) brightness(" + br.toString() + "%) saturate(" + sa.toString() + "%)";
  drawBackdrop();
  drawImg();
  drawBorder(10);
}


function brightnessGo() {
  /////////////////////////////////////
  ///  BRIGHTNESS ONLY ON CANVAS 2  ///
  /////////////////////////////////////
  ran2 = slide_button.value
  co = Math.round(100 * Math.pow(1.02, (50 - ran1)));
  br = Math.round(100 * Math.pow(1.02, (50 - ran2)));
  sa = Math.round(100 * Math.pow(1.02, (50 - ran3)));
  ctx2.filter = "contrast(" + co.toString() + "%) brightness(" + br.toString() + "%) saturate(" + sa.toString() + "%)";
  drawBackdrop();
  drawImg();
  drawBorder(10);
}


function colourGo() {
  /////////////////////////////////////
  ///  SATURATION ONLY ON CANVAS 2  ///
  /////////////////////////////////////
  ran3 = slide_button.value
  co = Math.round(100 * Math.pow(1.02, (50 - ran1)));
  br = Math.round(100 * Math.pow(1.02, (50 - ran2)));
  sa = Math.round(100 * Math.pow(1.02, (50 - ran3)));
  ctx2.filter = "contrast(" + co.toString() + "%) brightness(" + br.toString() + "%) saturate(" + sa.toString() + "%)";
  drawBackdrop();
  drawImg();
  drawBorder(10);
}


function loadIt() {
  load_button.click();
}



function pictureEnter() {
  //////////////////////////////////////////////////////
  ///   USE! (save and exit)   NEW TEMPORARY CANVAS  ///
  ////////////////////////////////////////////////////// 
  var sname = "IDCardPicture";
  var canvas1 = document.createElement('canvas');
  canvas1.width = 250;
  canvas1.height = 250;
  canvas1.getContext('2d').drawImage(canvas[0], portx1, porty1, portx2 - portx1, porty2 - porty1, 0, 0, 500, 500);
  var dataurl = canvas1.toDataURL();
  localStorage.setItem(sname, dataurl);
  //console.log(dataurl);
  window.open("411_generate.html", "_self");  //doesnt work on samsung A03 smartphone browser
}



///////////////////////////////////////////////////////
// Main application - run only once at load screen  ///
///////////////////////////////////////////////////////
function OnStart() {
  preload = false;
  
  /////////////////////////////////
  ///  load images if existing  ///
  /////////////////////////////////
  var picture = localStorage.getItem('IDCardPicture');
  var image;
  if (picture) {
    image = document.createElement('img');
    image.src = picture;
  }

  redraw(false, edge, image);


  load_button.addEventListener('change', handleFile, false);
  canvas[0].addEventListener("mousedown", doMouseDown, false);
  canvas[0].addEventListener("mousemove", doMouseMove, false);
  canvas[0].addEventListener("mouseup", doMouseUp, false);
  canvas[0].addEventListener("mouseout", doMouseOut, false);
  canvas[0].addEventListener('touchstart', sketchpad_touchStart, false);
  canvas[0].addEventListener('touchmove', sketchpad_touchMove, false);
  rotate_button.addEventListener('click', function () {
    rotate();
  });
  reset_button.addEventListener('click', function () {
    resetslides();
  });
  ///////////////////////
  ///  Slider (ZOOM)  ///
  ///////////////////////
  slide_button.oninput = function () {
    if (sliderselected == 1) { enlageGo() }
    if (sliderselected == 2) { contrastGo() }
    if (sliderselected == 3) { brightnessGo() }
    if (sliderselected == 4) { colourGo() }
  }
  rotate_button.classList.remove(rotate_button.classList.item(0))
  rotate_button.disabled = true;
  rotate_button.classList.add("disabled_button");
  reset_button.classList.remove(reset_button.classList.item(0))
  reset_button.disabled = true;
  reset_button.classList.add("disabled_button");
  document.getElementById("canvas").addEventListener("wheel", myFunction);
  document.getElementById("canvas").addEventListener("mousewheel", myFunction);
  document.getElementById("canvas").addEventListener("DOMMouseScroll", myFunction);
}

