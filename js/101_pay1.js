
var proposal = ""
var others = 0
var hash0 = ""
var hash1 = ""
var hash2 = ""
var hash3 = ""
var hash4 = ""
var hash5 = ""
var name0 = ""
var name1 = ""
var name2 = ""
var name3 = ""
var name4 = ""
var name5 = ""
var myName = ""
var total = 0
var xr0 = 0
var xr1 = 0
var xr2 = 0
var xr3 = 0
var xr4 = 0
var xr5 = 0
var securityHash = ""
var timestamp = ""
var currencyChar = "ᕫ"

// var senderIDCard = ""
// var senderIDCode = ""
// var senderName = ""
// var senderBlockchain = ""
// var senderCard = ""
// var myFile = ""
// var fileContent
// 

// var perc = 50
// var sortbut = 1

// var amountTot = 0
// var amount0 = 0
// var amount1 = 0
// var amount2 = 0
// var amount3 = 0
// var amount4 = 0
// var amount5 = 0
// var xamount1 = 0 //for manual function
// var xamount2 = 0 //for manual function
// var xamount3 = 0 //for manual function
// var xamount4 = 0 //for manual function
// var xamount5 = 0 //for manual function
// var rr0 = 0
// var rr1 = 0
// var rr2 = 0
// var rr3 = 0
// var rr4 = 0
// var rr5 = 0
// var arname = Array(5000).fill("")
// var arhash = Array(5000).fill("")
// var armyhash = Array(5000).fill("")
// var aramount = Array(5000).fill(0)
// var arcount = 0
// var armycount = 0
// 
// var allOthers = 0
// var senderPerc = 0.5
// var orig = "" //is used to copy data of originators to clipboard
// var securityhash = "" //is the security hash variable
// var hc = 0;   //hour counter, 2022-01-01 01:00 = 1 UCT (Coordinated Universal Time). The counter does not need to look at daylight saving time or time zones
// var hcd = 13; //day
// var hcm = 2; //month
// var hcy = 2035; //year
// var hch = 11; //hour
// var hcmin = 0 //minutes
// var hcsec = 0 //seconds
// var hcmsec = 0 //milliseconds
// var bdd = 13;   //birthday day
// var bmm = 2;    //birthday month
// var byy = 1965; //birthday year
// var bhh = 0;    //birthday hour
// //var MyBhh = 0;  //my birthday hour
// var timer = ""; //string to represent the progress in time in the DATASET


// const slide_button = document.getElementById("myRange");


////////////////////////////////////////////////////
//  Secure Hash Algorithm (SHA256)                //
//  http://www.webtoolkit.info/                   //
//                                                //
//  Original code by Angel Marin, Paul Johnston.  //
//                                                //
////////////////////////////////////////////////////
function SHA256(s){
    var chrsz   = 8;
    var hexcase = 0;
    function safe_add (x, y) {
        var lsw = (x & 0xFFFF) + (y & 0xFFFF);
        var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xFFFF);
    }
    function S (X, n) { return ( X >>> n ) | (X << (32 - n)); }
    function R (X, n) { return ( X >>> n ); }
    function Ch(x, y, z) { return ((x & y) ^ ((~x) & z)); }
    function Maj(x, y, z) { return ((x & y) ^ (x & z) ^ (y & z)); }
    function Sigma0256(x) { return (S(x, 2) ^ S(x, 13) ^ S(x, 22)); }
    function Sigma1256(x) { return (S(x, 6) ^ S(x, 11) ^ S(x, 25)); }
    function Gamma0256(x) { return (S(x, 7) ^ S(x, 18) ^ R(x, 3)); }
    function Gamma1256(x) { return (S(x, 17) ^ S(x, 19) ^ R(x, 10)); }
    function core_sha256 (m, l) {
        var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786, 0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA, 0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070, 0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2);
        var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19);
        var W = new Array(64);
        var a, b, c, d, e, f, g, h, i, j;
        var T1, T2;
        m[l >> 5] |= 0x80 << (24 - l % 32);
        m[((l + 64 >> 9) << 4) + 15] = l;
        for ( var i = 0; i<m.length; i+=16 ) {
            a = HASH[0];
            b = HASH[1];
            c = HASH[2];
            d = HASH[3];
            e = HASH[4];
            f = HASH[5];
            g = HASH[6];
            h = HASH[7];
            for ( var j = 0; j<64; j++) {
                if (j < 16) W[j] = m[j + i];
                else W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16]);
                T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j]);
                T2 = safe_add(Sigma0256(a), Maj(a, b, c));
                h = g;
                g = f;
                f = e;
                e = safe_add(d, T1);
                d = c;
                c = b;
                b = a;
                a = safe_add(T1, T2);
            }
            HASH[0] = safe_add(a, HASH[0]);
            HASH[1] = safe_add(b, HASH[1]);
            HASH[2] = safe_add(c, HASH[2]);
            HASH[3] = safe_add(d, HASH[3]);
            HASH[4] = safe_add(e, HASH[4]);
            HASH[5] = safe_add(f, HASH[5]);
            HASH[6] = safe_add(g, HASH[6]);
            HASH[7] = safe_add(h, HASH[7]);
        }
        return HASH;
    }
    function str2binb (str) {
        var bin = Array();
        var mask = (1 << chrsz) - 1;
        for(var i = 0; i < str.length * chrsz; i += chrsz) {
            bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i%32);
        }
        return bin;
    }
    function Utf8Encode(string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    }
    function binb2hex (binarray) {
        var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
        var str = "";
        for(var i = 0; i < binarray.length * 4; i++) {
            str += hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8+4)) & 0xF) +
            hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8  )) & 0xF);
        }
        return str;
    }
    s = Utf8Encode(s);
    return binb2hex(core_sha256(str2binb(s), s.length * chrsz));
}


const format = (num, decimals) => num.toLocaleString('en-US', {
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});


function setDate()
{
    ///////////////////////////////////////////////////////
    // based on hc, the vars hcd,hcm,hcy and hch are set //
    // the date in hc_date is also set                   //
    ///////////////////////////////////////////////////////
    var t1 = 0;
    var t5 = 0;
    hcd = Math.trunc(hc/24);                          //hcd = daynumber since 01-01-2022 where 01-01-2022 is day 0 here
    hch = hc - (24*hcd);                              //hch = number of hours on day t1
    t1 = Math.trunc(hcd / 1461);                      //t1 = number of clusters of 4 years (=1461 days) that are past
    hcd = 1 + (hcd - (1461 * t1));                    //hcd = remaining number of days in remaining cluster, add one day to make 01-01-2022 day 1
    t5 = 0;                                           //t5 = number of years that has past in the remaining cluster 
    if (hcd > 365) {t5++;hcd=hcd-365;};               //t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
    if ((hcd > 365) && (t5 == 1)) {t5++;hcd=hcd-365;};//t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
    if ((hcd > 366)  && (t5 == 2)) {t5++;hcd=hcd-366;};//t5 = number of years that has past in the remaining cluster, hcd remaining number of days in remaining part of cluster
    hcy = 2022 + t5 + (4*t1);
    //hcd is now the day number in year hcy
    hcm = 1;
    if (hcd > 31) {hcm=2;hcd=hcd-31;};
    if (t5 == 2)
    {
       if ((hcd > 29) && (hcm>1)) {hcm=3;hcd=hcd-29;};
    } else
    {    
       if ((hcd > 28) && (hcm>1)) {hcm=3;hcd=hcd-28;};
    };
    if ((hcd > 31) && (hcm>2)) {hcm=4;hcd=hcd-31;};
    if ((hcd > 30) && (hcm>3)) {hcm=5;hcd=hcd-30;};
    if ((hcd > 31) && (hcm>4)) {hcm=6;hcd=hcd-31;};
    if ((hcd > 30) && (hcm>5)) {hcm=7;hcd=hcd-30;};
    if ((hcd > 31) && (hcm>6)) {hcm=8;hcd=hcd-31;};
    if ((hcd > 31) && (hcm>7)) {hcm=9;hcd=hcd-31;};
    if ((hcd > 30) && (hcm>8)) {hcm=10;hcd=hcd-30;};
    if ((hcd > 31) && (hcm>9)) {hcm=11;hcd=hcd-31;};
    if ((hcd > 30) && (hcm>10)) {hcm=12;hcd=hcd-30;};
    var mnd = "Jan";
    switch(hcm) 
    {
        case 1:  {var mnd = "Jan";} break;        
        case 2:  {var mnd = "Feb";} break;        
        case 3:  {var mnd = "Mar";} break;        
        case 4:  {var mnd = "Apr";} break;        
        case 5:  {var mnd = "May";} break;        
        case 6:  {var mnd = "Jun";} break;        
        case 7:  {var mnd = "Jul";} break;        
        case 8:  {var mnd = "Aug";} break;        
        case 9:  {var mnd = "Sep";} break;        
        case 10:  {var mnd = "Oct";} break;        
        case 11:  {var mnd = "Nov";} break;        
        case 12:  {var mnd = "Dec";} break;        
    };
    day = (hcd < 10 ? "0" : "") + hcd;
    hour = (hch < 10 ? "0" : "") + hch;
    timer = day + mnd + hcy+"_"+hour+"u";
}


function getDate()
{
    ///////////////////////////////////////////////////////
    // based on the vars hcd,hcm,hcy and hch             //
    // the hour count in hc is calculated                //
    ///////////////////////////////////////////////////////
    
    ///////////////////////////////////////////
    /// first count all extra jump-year days///
    ///////////////////////////////////////////
    var t1 = 0;
    t1 = Math.trunc((hcy - 2021) / 4);    //in the year 2025 we need to start with one jump day
    if (((Math.trunc((hcy - 2024) / 4)) == ((hcy - 2024) / 4)) && (hcm > 2))  {t1++;}; //if feb has pars in a jump year we also need to add one jump day
    t1 = hcd + t1 + (365*(hcy-2022)); //then we add the dayofmonth plus 365xnumber of days in the prev. years from 2022
    if (hcm > 1) {t1 = t1+31;}; //then we add the days of all past months in this year vvv
    if (hcm > 2) {t1 = t1+28;};
    if (hcm > 3) {t1 = t1+31;};
    if (hcm > 4) {t1 = t1+30;};
    if (hcm > 5) {t1 = t1+31;};
    if (hcm > 6) {t1 = t1+30;};
    if (hcm > 7) {t1 = t1+31;};
    if (hcm > 8) {t1 = t1+31;};
    if (hcm > 9) {t1 = t1+30;};
    if (hcm > 10) {t1 = t1+31;};
    if (hcm > 11) {t1 = t1+30;};
    hc = (24*(t1-1)) + hch; //because the day has not passed we reduce one day, multiply with 24 and add the current hour.
    //var ss = String(hc);
    //document.getElementById("ta1").innerHTML ="Hour Count: " + ss;
}



function OnStart()
{
    document.getElementById("loadtext").addEventListener('change', handleFile, false);
    ////////////////////////////////////////////////////////////////////////////
    ///  if payment info is already loaded but not finalised and deleted we  ///
    ///  skip this part of the process so we dont interfere with the non-    ///
    ///  processed data of the previous payment                              ///
    ////////////////////////////////////////////////////////////////////////////
    s1 = window.localStorage.getItem('myName')
    if (s1 == null) {s1 = ""}
    if ((s1 != s1) || (s1 == "")) //catch a NaN issue
    {
        alert("You first need to enter your IDCard.")
        window.open("400_idcard.html","_self");
    }
    else
    {
        myName = s1
        document.getElementById("acceptPay").classList.remove(document.getElementById("acceptPay").classList.item(0))
        var s1 = localStorage.getItem("proposal");
        if (s1 == null) {s1 = ""}
        if ((s1 != s1) || (s1 == "")) //catch a NaN issue
        {
            document.getElementById("acceptPay").disabled = true;
            document.getElementById("acceptPay").classList.add("disabled_button");
            alert("The proposal has not been found. Please select it again.")
            window.open("100_pay.html","_self");
        }
        else
        {
            document.getElementById("acceptPay").disabled = false;
            document.getElementById("acceptPay").classList.add("main_button");
            /////////////////////////////////////////////////////////////////////////////////////////////
            ///  0|55|Gloria Moses|86ad9496e549961f84c4f06f9a00b08d42412ee99fd3b9ab069219a35b0c9982|  ///
            ///  8e9b492cf19d73c0e71ebaa8f14ee2f9a5c30d73a8198addddc3a107ae458cce>4564:1234567<       ///
            /////////////////////////////////////////////////////////////////////////////////////////////
            proposal = s1
            ////////////////////////////////////
            ///  arrange currency character  ///
            ////////////////////////////////////
            s1 = window.localStorage.getItem('myCurrencyChar')
            if (s1 == null) {s1 = ""}
            if ((s1 != s1) || (s1 == "")) //catch a NaN issue
            {
                document.getElementById("curchar").innerHTML = "ᕫ"
                window.localStorage.setItem('myCurrencyChar',"ᕫ")
                currencyChar = "ᕫ"
            }
            else
            {
                document.getElementById("curchar").innerHTML = s1
                currencyChar = s1
            }
            /////////////////////////////////////
            ///  read the proposal in detail  /// 
            /////////////////////////////////////
            var ssr = ""
            var sss = proposal
            var t0 = sss.indexOf("|")
            others = Number(sss.substring(0,t0))     //number of others
            /////////////////////////////////////
            sss = sss.substring(t0+1)
            t0 = sss.indexOf("|")
            ssr = sss.substring(0,t0) //payment amount of personal coins of the person that pays
            xr0 = Number(ssr) //amount payment person0
            /////////////////////////////////////
            sss = sss.substring(t0+1)
            t0 = sss.indexOf("|")
            name0 = sss.substring(0,t0) //name0 of the person that pays
            /////////////////////////////////////
            sss = sss.substring(t0+1)
            t0 = sss.indexOf("|")
            hash0 = sss.substring(0,t0) //hash0 of the person that pays
            /////////////////////////////////////
            if (others > 0)
            {
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                ssr = sss.substring(0,t0) //payment amount of personal coins of the person that pays
                xr1 = Number(ssr) //amount payment person1
                /////////////////////////////////////
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                name1 = sss.substring(0,t0) //name1 of the person that pays
                /////////////////////////////////////
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                hash1 = sss.substring(0,t0) //hash1 of the person that pays
                /////////////////////////////////////
            }
            /////////////////////////////////////
            if (others > 1)
            {
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                ssr = sss.substring(0,t0) //payment amount of personal coins of the person that pays
                xr2 = Number(ssr) //amount payment person2
                /////////////////////////////////////
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                name2 = sss.substring(0,t0) //name2 of the person that pays
                /////////////////////////////////////
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                hash2 = sss.substring(0,t0) //hash2 of the person that pays
                /////////////////////////////////////
            }
            /////////////////////////////////////
            if (others > 2)
            {
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                ssr = sss.substring(0,t0) //payment amount of personal coins of the person that pays
                xr3 = Number(ssr) //amount payment person3
                /////////////////////////////////////
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                name3 = sss.substring(0,t0) //name3 of the person that pays
                /////////////////////////////////////
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                hash3 = sss.substring(0,t0) //hash3 of the person that pays
                /////////////////////////////////////
            }
            /////////////////////////////////////
            if (others > 3)
            {
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                ssr = sss.substring(0,t0) //payment amount of personal coins of the person that pays
                xr4 = Number(ssr) //amount payment person4
                /////////////////////////////////////
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                name4 = sss.substring(0,t0) //name4 of the person that pays
                /////////////////////////////////////
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                hash4 = sss.substring(0,t0) //hash4 of the person that pays
                /////////////////////////////////////
            }
            /////////////////////////////////////
            if (others > 4)
            {
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                ssr = sss.substring(0,t0) //payment amount of personal coins of the person that pays
                xr5 = Number(ssr) //amount payment person5
                /////////////////////////////////////
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                name5 = sss.substring(0,t0) //name5 of the person that pays
                /////////////////////////////////////
                sss = sss.substring(t0+1)
                t0 = sss.indexOf("|")
                hash5 = sss.substring(0,t0) //hash5 of the person that pays
                /////////////////////////////////////
            }
            ///  t0 is now the position of the separator | before the securityhas|timestamp string
            ///  ...|8e9b492cf19d73c0e71ebaa8f14ee2f9a5c30d73a8198addddc3a107ae458cce>4564:1234567<       ///
            sss = sss.substring(t0+1)
            if (sss.length > 60)
            {
                securityHash = sss.substring(0,64)
                timestamp = sss.substring(65,77)
            }
            else
            {
                securityHash = ""
                timestamp = sss.substring(1,13)
            }
            //alert("..."+securityHash+"..."+timestamp+"...")
            var ss0 = format(xr0)+" "+currencyChar
            var ss1 = format(xr1)+" "+currencyChar
            var ss2 = format(xr2)+" "+currencyChar
            var ss3 = format(xr3)+" "+currencyChar
            var ss4 = format(xr4)+" "+currencyChar
            var ss5 = format(xr5)+" "+currencyChar
            document.getElementById("name0").innerHTML = myName
            document.getElementById("amount0").innerHTML = ss0
            var ss6 = ""
            if (others == 0)
            {
                document.getElementById("mutual").style.visibility = "hidden"
                document.getElementById("name1").style.visibility = "hidden"
                document.getElementById("amount1").style.visibility = "hidden"
                document.getElementById("name2").style.visibility = "hidden"
                document.getElementById("amount2").style.visibility = "hidden"
                document.getElementById("name3").style.visibility = "hidden"
                document.getElementById("amount3").style.visibility = "hidden"
                document.getElementById("name4").style.visibility = "hidden"
                document.getElementById("amount4").style.visibility = "hidden"
                document.getElementById("name5").style.visibility = "hidden"
                document.getElementById("amount5").style.visibility = "hidden"
                document.getElementById("stripe0").style.visibility = "hidden"
                document.getElementById("stripe1").style.visibility = "hidden"
                document.getElementById("total").style.visibility = "hidden"
                document.getElementById("amount6").style.visibility = "hidden"
            }
            if (others == 1)
            {
                document.getElementById("mutual").style.visibility = "visible"
                document.getElementById("name1").style.visibility = "visible"
                document.getElementById("amount1").style.visibility = "visible"
                document.getElementById("name2").style.visibility = "visible"
                document.getElementById("amount2").style.visibility = "visible"
                document.getElementById("name3").style.visibility = "visible"
                document.getElementById("amount3").style.visibility = "visible"
                document.getElementById("name4").style.visibility = "hidden"
                document.getElementById("amount4").style.visibility = "hidden"
                document.getElementById("name5").style.visibility = "hidden"
                document.getElementById("amount5").style.visibility = "hidden"
                document.getElementById("stripe0").style.visibility = "hidden"
                document.getElementById("stripe1").style.visibility = "hidden"
                document.getElementById("total").style.visibility = "hidden"
                document.getElementById("amount6").style.visibility = "hidden"
                document.getElementById("name2").style.color = "#cccccc"
                document.getElementById("amount2").style.color = "#cccccc"
                document.getElementById("name3").style.color = "#F7E273"
                document.getElementById("amount3").style.color = "#F7E273"
                document.getElementById("name1").innerHTML = name1
                document.getElementById("amount1").innerHTML = ss1
                document.getElementById("name2").innerHTML = ""
                document.getElementById("amount2").innerHTML = "<small>════════════</small>"
                document.getElementById("name3").innerHTML = "Total"
                ss6 = format((xr0+xr1))+" "+currencyChar
                document.getElementById("amount3").innerHTML = ss6
            }
            if (others == 2)
            {
                document.getElementById("mutual").style.visibility = "visible"
                document.getElementById("name1").style.visibility = "visible"
                document.getElementById("amount1").style.visibility = "visible"
                document.getElementById("name2").style.visibility = "visible"
                document.getElementById("amount2").style.visibility = "visible"
                document.getElementById("name3").style.visibility = "visible"
                document.getElementById("amount3").style.visibility = "visible"
                document.getElementById("name4").style.visibility = "visible"
                document.getElementById("amount4").style.visibility = "visible"
                document.getElementById("name5").style.visibility = "hidden"
                document.getElementById("amount5").style.visibility = "hidden"
                document.getElementById("stripe0").style.visibility = "hidden"
                document.getElementById("stripe1").style.visibility = "hidden"
                document.getElementById("total").style.visibility = "hidden"
                document.getElementById("amount6").style.visibility = "hidden"
                document.getElementById("name2").style.color = "#cccccc"
                document.getElementById("amount2").style.color = "#cccccc"
                document.getElementById("name3").style.color = "#cccccc"
                document.getElementById("amount3").style.color = "#cccccc"
                document.getElementById("name4").style.color = "#F7E273"
                document.getElementById("amount4").style.color = "#F7E273"
                document.getElementById("name1").innerHTML = name1
                document.getElementById("amount1").innerHTML = ss1
                document.getElementById("name2").innerHTML = name2
                document.getElementById("amount2").innerHTML = ss2
                document.getElementById("name3").innerHTML = ""
                document.getElementById("amount3").innerHTML = "<small>════════════</small>"
                document.getElementById("name4").innerHTML = "Total"
                ss6 = format((xr0+xr1+xr2))+" "+currencyChar
                document.getElementById("amount4").innerHTML = ss6 
            }
            if (others == 3)
            {
                document.getElementById("mutual").style.visibility = "visible"
                document.getElementById("name1").style.visibility = "visible"
                document.getElementById("amount1").style.visibility = "visible"
                document.getElementById("name2").style.visibility = "visible"
                document.getElementById("amount2").style.visibility = "visible"
                document.getElementById("name3").style.visibility = "visible"
                document.getElementById("amount3").style.visibility = "visible"
                document.getElementById("name4").style.visibility = "visible"
                document.getElementById("amount4").style.visibility = "visible"
                document.getElementById("name5").style.visibility = "visible"
                document.getElementById("amount5").style.visibility = "visible"
                document.getElementById("stripe0").style.visibility = "hidden"
                document.getElementById("stripe1").style.visibility = "hidden"
                document.getElementById("total").style.visibility = "hidden"
                document.getElementById("amount6").style.visibility = "hidden"
                document.getElementById("name2").style.color = "#cccccc"
                document.getElementById("amount2").style.color = "#cccccc"
                document.getElementById("name3").style.color = "#cccccc"
                document.getElementById("amount3").style.color = "#cccccc"
                document.getElementById("name4").style.color = "#cccccc"
                document.getElementById("amount4").style.color = "#ccccc"
                document.getElementById("name5").style.color = "#F7E273"
                document.getElementById("amount5").style.color = "#F7E273"
                document.getElementById("name1").innerHTML = name1
                document.getElementById("amount1").innerHTML = ss1
                document.getElementById("name2").innerHTML = name2
                document.getElementById("amount2").innerHTML = ss2
                document.getElementById("name3").innerHTML = name3
                document.getElementById("amount3").innerHTML = ss3
                document.getElementById("name4").innerHTML = ""
                document.getElementById("amount4").innerHTML = "<small>════════════</small>"
                document.getElementById("name5").innerHTML = "Total"
                ss6 = format((xr0+xr1+xr2+xr3))+" "+currencyChar
                document.getElementById("amount5").innerHTML = ss6
            }
            if (others == 4)
            {
                document.getElementById("mutual").style.visibility = "visible"
                document.getElementById("name1").style.visibility = "visible"
                document.getElementById("amount1").style.visibility = "visible"
                document.getElementById("name2").style.visibility = "visible"
                document.getElementById("amount2").style.visibility = "visible"
                document.getElementById("name3").style.visibility = "visible"
                document.getElementById("amount3").style.visibility = "visible"
                document.getElementById("name4").style.visibility = "visible"
                document.getElementById("amount4").style.visibility = "visible"
                document.getElementById("name5").style.visibility = "visible"
                document.getElementById("amount5").style.visibility = "visible"
                document.getElementById("stripe0").style.visibility = "visible"
                document.getElementById("stripe1").style.visibility = "visible"
                document.getElementById("total").style.visibility = "hidden"
                document.getElementById("amount6").style.visibility = "hidden"
                document.getElementById("name2").style.color = "#cccccc"
                document.getElementById("amount2").style.color = "#cccccc"
                document.getElementById("name3").style.color = "#cccccc"
                document.getElementById("amount3").style.color = "#cccccc"
                document.getElementById("name4").style.color = "#cccccc"
                document.getElementById("amount4").style.color = "#cccccc"
                document.getElementById("name5").style.color = "#cccccc"
                document.getElementById("amount5").style.color = "#cccccc"
                document.getElementById("stripe0").style.color = "#F7E273"
                document.getElementById("stripe1").style.color = "#F7E273"
                document.getElementById("name1").innerHTML = name1
                document.getElementById("amount1").innerHTML = ss1
                document.getElementById("name2").innerHTML = name2
                document.getElementById("amount2").innerHTML = ss2
                document.getElementById("name3").innerHTML = name3
                document.getElementById("amount3").innerHTML = ss3
                document.getElementById("name4").innerHTML = name4
                document.getElementById("amount4").innerHTML = ss4
                document.getElementById("name5").innerHTML = ""
                document.getElementById("amount5").innerHTML = "<small>════════════</small>"
                document.getElementById("stripe0").innerHTML = "Total"
                ss6 = format((xr0+xr1+xr2+xr3+xr4))+" "+currencyChar
                document.getElementById("stripe1").innerHTML = ss6
            }
            if (others == 5)
            {
                document.getElementById("mutual").style.visibility = "visible"
                document.getElementById("name1").style.visibility = "visible"
                document.getElementById("amount1").style.visibility = "visible"
                document.getElementById("name2").style.visibility = "visible"
                document.getElementById("amount2").style.visibility = "visible"
                document.getElementById("name3").style.visibility = "visible"
                document.getElementById("amount3").style.visibility = "visible"
                document.getElementById("name4").style.visibility = "visible"
                document.getElementById("amount4").style.visibility = "visible"
                document.getElementById("name5").style.visibility = "visible"
                document.getElementById("amount5").style.visibility = "visible"
                document.getElementById("stripe0").style.visibility = "visible"
                document.getElementById("stripe1").style.visibility = "visible"
                document.getElementById("total").style.visibility = "visible"
                document.getElementById("amount6").style.visibility = "visible"
                document.getElementById("name2").style.color = "#cccccc"
                document.getElementById("amount2").style.color = "#cccccc"
                document.getElementById("name3").style.color = "#cccccc"
                document.getElementById("amount3").style.color = "#cccccc"
                document.getElementById("name4").style.color = "#cccccc"
                document.getElementById("amount4").style.color = "#cccccc"
                document.getElementById("name5").style.color = "#cccccc"
                document.getElementById("amount5").style.color = "#cccccc"
                document.getElementById("stripe0").style.color = "#cccccc"
                document.getElementById("stripe1").style.color = "#cccccc"
                document.getElementById("total").style.color = "#F7E273"
                document.getElementById("amount6").style.color = "#F7E273"

                document.getElementById("name1").innerHTML = name1
                document.getElementById("amount1").innerHTML = ss1
                document.getElementById("name2").innerHTML = name2
                document.getElementById("amount2").innerHTML = ss2
                document.getElementById("name3").innerHTML = name3
                document.getElementById("amount3").innerHTML = ss3
                document.getElementById("name4").innerHTML = name4
                document.getElementById("amount4").innerHTML = ss4
                document.getElementById("name5").innerHTML = name5
                document.getElementById("amount5").innerHTML = ss5
                ss6 = format((xr0+xr1+xr2+xr3+xr4+xr5))+" "+currencyChar
                document.getElementById("amount6").innerHTML = ss6
            }
            ss6 = format((xr0+xr1+xr2+xr3+xr4+xr5))
            document.getElementById("amount").innerHTML = ss6 
            document.getElementById("tobepaid").innerHTML = "To be paid to "+name0 
        }
    }
}


function handleFile(e) 
{
    var fileSize = 0;
    var theFile = document.getElementById("loadtext"); 
    if (typeof (FileReader) != "undefined") 
    {
       //get table element
        var table = document.getElementById("myTable");
        var headerLine = "";
        //create html5 file reader object
        var myReader = new FileReader();
        // call filereader. onload function
        myReader.onload = function(e) 
        {
            fileContent = myReader.result;
            processSecurityHash()
        }
        //call file reader onload
        myReader.readAsText(theFile.files[0]);
    }
    else 
    {
        alert("This browser does not support HTML5.");
    }
    return false;
}


function loadIt()
{
  document.getElementById("loadtext").click();
}


function processSecurityHash()
{
    var s1 = ""
    var s2 = ""
    var ss = ""
    // s1 = "<small>This is the hash of the Security Image you just uploaded.<br>"+
    //     "It will be added to your payment proposal.</small>"
    // document.getElementById("uploadtext").innerHTML = s1
    ////////////////////////////////////////////////////////////////////////////////
    ///  first count the number of previous IDCards, when a person has multiple  ///
    ///  IDCards they are all included in the payment_id file, at the end,       ///
    ///  starting with the "%" character                                         ///
    ////////////////////////////////////////////////////////////////////////////////
    //console.log(fileContent);
    securityHash = SHA256(fileContent)
    window.localStorage.setItem('securityHash',securityHash)
    s1 = "<font color=#cccccc><small>"+"Please check this Security Image hash:<br></font><font color=#AD8F00>"+securityHash.substring(0,32)+
            "<br>"+securityHash.substring(32,64)+"<br><small>&nbsp;</small></small></font>"
    document.getElementById("uploadtext").innerHTML = s1
    document.getElementById("uploadtext").style ="text-align: center"
    document.getElementById("row0").style.height ="18px"
    document.getElementById("uploadSecurityImage").style.display = "none"
}


function acceptPay()
{
    var myCurrentHash = ""
    var s1 = window.localStorage.getItem('myCurrentHash')
    if (s1 == null) {s1 = ""}
    if ((s1 != s1) || (s1 == "")) //catch a NaN issue
    {
        alert("You don't have a proper IDCard. You are directed to 'IDCard' to set your IDCard.")
        window.localStorage.removeItem('securityHash');
        window.localStorage.removeItem('proposal')
        window.open("400_idcard.html","_self");
    }
    else
    {
        myCurrentHash = s1
        var s2 = format((xr0+xr1+xr2+xr3+xr4+xr5))
        var s1 = "Are you sure to transfer "+s2+" "+currencyChar+" to "+name0+" in the way it is proposed to you?"
        if (window.confirm(s1)) 
        {
            var myFiles = ""  //to get the filename of your blockchain
            var read = "" //to read your blockchain (person that pays)
            var read2 = "" //to read blockchain of person that receives
            s1 = window.localStorage.getItem('myFiles')
            if (s1 == null) {s1 = ""}
            if ((s1 != s1) || (s1 == "")) //catch a NaN issue, this shouldnt happen in this stage
            {
                read = ""
                alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.")
                window.localStorage.removeItem('securityHash');
                window.localStorage.removeItem('proposal')
                window.open("400_idcard.html","_self");
            }
            else
            {
                var myBlockchain = s1 + ".txt"
                s1 = window.localStorage.getItem(myBlockchain)
                if (s1 == null) {s1 = ""}
                if ((s1 != s1) || (s1 == "")) //catch a NaN issue, this shouldnt happen in this stage
                {
                    read = ""
                    alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.")
                    window.localStorage.removeItem('securityHash');
                    window.localStorage.removeItem('proposal')
                    window.open("400_idcard.html","_self");
                }
                else
                {
                    read = s1.trim()  //person that pays (me)
                    s1 = hash0+".txt"
                    //alert(s1)
                    var s1 = window.localStorage.getItem(s1)  //86ad9496e549961f84c4f06f9a00b08d42412ee99fd3b9ab069219a35b0c9982.txt
                    if (s1 == null) {s1 = ""}
                    if ((s1 != s1) || (s1 == "")) //catch a NaN issue, this shouldnt happen in this stage
                    {
                        read2 = ""
                        alert("You didn't upload the proposal properly. You will be asked to set it again.")
                        window.localStorage.removeItem('securityHash');
                        window.localStorage.removeItem('proposal')
                        window.open("100_pay.html","_self");
                    }
                    else
                    {
                        read2 = s1.trim()  //blockchain of receiver
                        /////////////////////////////////////////
                        ///  but first we start with read     ///
                        ///  change blockchain of payer (me)  ///
                        /////////////////////////////////////////
                        var ss = "S" + hash0 + "|" + timestamp + "|" + securityHash + "|" + String(xr0)
                        if (others>0) { ss = ss + "|" + hash1 + "|" + String(xr1) }
                        if (others>1) { ss = ss + "|" + hash2 + "|" + String(xr2) }
                        if (others>2) { ss = ss + "|" + hash3 + "|" + String(xr3) }
                        if (others>3) { ss = ss + "|" + hash4 + "|" + String(xr4) }
                        if (others>4) { ss = ss + "|" + hash5 + "|" + String(xr5) }
                        ss = ss + "|"
                        read = read + ss
                        var endhash = SHA256(read)
                        read = read + endhash + "<"
                        window.localStorage.setItem(myBlockchain,read)
                        /////////////////////////////////////////////////////////////////////
                        /// change my copy of the blockchain of the person that receives  ///
                        /////////////////////////////////////////////////////////////////////
                        var ss = "R" + myCurrentHash + "|" + timestamp + "|" + securityHash + "|" + String(xr0)
                        if (others>0) { ss = ss + "|" + hash1 + "|" + String(xr1) }
                        if (others>1) { ss = ss + "|" + hash2 + "|" + String(xr2) }
                        if (others>2) { ss = ss + "|" + hash3 + "|" + String(xr3) }
                        if (others>3) { ss = ss + "|" + hash4 + "|" + String(xr4) }
                        if (others>4) { ss = ss + "|" + hash5 + "|" + String(xr5) }
                        ss = ss + "|"
                        read2 = read2 + ss
                        var endhash = SHA256(read2)
                        read2 = read2 + endhash + "<"
                        ss = hash0+".txt"
                        window.localStorage.setItem(ss,read2)
                        //////////////////////////////
                        ///  remove proposal data  ///
                        //////////////////////////////
                        window.localStorage.removeItem('securityHash');
                        window.localStorage.removeItem('proposal')
                        s2 = "The payment process is finalized and cleared." 
                        alert(s2)
                        window.open("300_cashflow.html","_self");
                    }
                }
            }
        }
    }
}
