
function OnStart() {
  var name = window.localStorage.getItem('myName')
  if (name == null) { name = "" }
  document.getElementById("name").value = name
  document.getElementById("name").focus()
  nameTyped()
}


function nameTyped() {
  var ss = ''
  var i = 0
  var tt = 0
  var t1 = 0
  var t2 = 0
  var t3 = 0
  var capit1 = false
  var capit2 = false
  var s1 = ""

  var err1 = false //min 2 words
  var err2 = false //between 5 and 32 char
  var err3 = false //no _ characters

  var name = document.getElementById("name").value
  //alert(name)
  if (!name || name.length > 0) {
    /////////////////////////////////////////
    /// remove all spaces from the start  ///
    /////////////////////////////////////////
    while ((name.length > 0) && ((name.charCodeAt(0) == 32) || (name.charCodeAt(0) == 39) || (name.charCodeAt(0) == 45))) {
      if (name.length > 1) {
        name = name.substring(1, name.length)
      }
      else {
        name = ""
      }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///  now we check the length (trimmed from spaces) on the back (spaces on the front are already removed)  ///
    ///  because we don't want to destroy the last space (the person is still typing) we use the ss var       ///
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ss = name.trim()
    tt = ss.length
    if ((tt < 5) || (tt > 32)) {
      err2 = true
    }
    /////////////////////////////////////////////
    /// now we check on "illegal" characters  ///
    /////////////////////////////////////////////
    tt = name.length
    if (tt > 0) {
      if (tt > 1) {
        ///////////////////////////////////////////////////////////////////
        ///  all procedures here have a name.length > 1                 ///
        ///////////////////////////////////////////////////////////////////
        /// allowed are:                                                ///
        /// ' ' (32) No 2 space allowed next to eachother               ///
        /// ' (39)  No 2 ' allowed next to eachother                    ///
        /// - (45)   No 2 - allowed next to eachother                   ///
        /// A-Z (65-90) No 2 capitals allowed next to eachother         ///
        /// a-z (97-122)                                                ///
        /// A.. (192-214) No 2 capitals allowed next to eachother       ///
        /// O.. (216-222) No 2 capitals allowed next to eachother       ///
        /// b.. (223-246)                                               ///
        /// u.. (248-255)                                               ///
        ///////////////////////////////////////////////////////////////////
        /// first we replace all not allowed char with "_"              ///
        ///////////////////////////////////////////////////////////////////
        for (i = 0; i < tt; i++) {
          code = name.charCodeAt(i);
          if ((code < 32) ||
            ((code > 32) && (code < 39)) ||
            ((code > 39) && (code < 44)) ||
            ((code > 46) && (code < 65)) ||
            ((code > 90) && (code < 97)) ||
            ((code > 122) && (code < 192)) ||
            ((code > 214) && (code < 216)) ||
            ((code > 246) && (code < 248)) ||
            (code > 255)) {
            ss = ""
            if (i > 0) {
              ss = name.substring(0, i)
            }
            ss = ss + "_"
            if (i < (tt - 1)) {
              ss = ss + name.substr(i + 1)
            }
            name = ss
            err3 = true
          }
        }
        ///////////////////////////////////////////////////////
        ///  now we check if 2 following chars are allowed  ///
        ///  2 upcase> 2nd becomes lowcase                  ///
        ///  2 space / - / ' last is deleted                ///
        ///////////////////////////////////////////////////////

        if ((tt > 4) || (tt < 33)) {
          ////////////////////////////////////////////////
          ///  first we deal with the uppercase issue  ///
          ///  double capitals next to eacother are    ///
          ///  not allowed and more than 2 capitals    ///
          ///  in one word is also not allowed         ///
          ////////////////////////////////////////////////
          t3 = 0
          ss = name.substring(0, 1) //ss = assemblystring with the proper chars in it
          for (i = 1; i < tt; i++) {
            t1 = ss.charCodeAt(ss.length - 1);
            if ((t1 == 32) || (t1 == 39) || (t1 == 45)) {
              t3 = 0
            }
            capit1 = (((t1 > 64) && (t1 < 91)) ||
              ((t1 > 191) && (t1 < 215)) ||
              ((t1 > 215) && (t1 < 223)))  //capit1 is if the last char of the assembly string is a capital
            s1 = name.substring(i, i + 1)
            t2 = name.charCodeAt(i);
            capit2 = (((t2 > 64) && (t2 < 91)) ||
              ((t2 > 191) && (t2 < 215)) ||
              ((t2 > 215) && (t2 < 223))) //capit2 is if the last char of name at 1 position is a capital
            s1 = name.substring(i, i + 1)

            // if (i==6)
            // {
            //   alert(capit1+"  "+capit2)
            // }
            if (capit1) {
              t3++
            }
            if (t3 > 1) {
              s1 = s1.toLowerCase()
              capit2 = false
            }

            if ((capit1) && (capit2)) {
              s1 = s1.toLowerCase()
              t3++
            }
            ss = ss + s1
          }
          name = ss
          ///////////////////////////////////////////////////////////////////////////////
          ///  then a capital preceding a space, quote or hyphen is also not allowed  /// 
          ///  and will automatically be changed in a loverkey character              ///
          ///  this only applies for a name.length>2                                  ///
          ///////////////////////////////////////////////////////////////////////////////
          //alert(name)
          if (name.length > 2) {
            ss = name.substring(0, 2) //ss = assemblystring with the proper chars in it
            for (i = 2; i < tt; i++) {
              s1 = ss.substring(ss.length - 1, ss.length) //is the last char of the on capital tested assemblystring
              t1 = s1.charCodeAt(0);      //is the code of the on capital tested char
              s2 = name.substring(i, i + 1) //this next char in name that must be tested if its a space, quote or hyphen
              t2 = name.charCodeAt(i);
              //alert("s1:"+s1+" s2:"+s2)
              if ((t2 == 32) || (t2 == 39) || (t2 == 45)) {
                capit1 = (((t1 > 64) && (t1 < 91)) ||
                  ((t1 > 191) && (t1 < 215)) ||
                  ((t1 > 215) && (t1 < 223)))  //capit2 is if the last char of the assembly string is a capital
                if (capit1) {
                  //alert("s1:"+s1+" s2:"+s2)
                  //s1 (or the last char of the on capital tested assemblystring) is an Capital and should be lowkey
                  ss = ss.substring(0, ss.length - 1) + s1.toLowerCase() + s2
                }
                else {
                  ss = ss + s2
                  //alert("s1:"+s1+" s2:"+s2)
                }
              }
              else {
                ss = ss + s2
                //alert("B:"+s2);
              }
            }
          }
          else {
            //name.length = 2: in this case the first char is always a Uppercase
            s1 = name.substring(0, 1)   //ss = assemblystring with the proper chars in it
            s2 = name.substring(1, 2) //this next char in name that must be tested if its a space, quote or hyphen
            t2 = name.charCodeAt(i);
            if ((t2 == 32) || (t2 == 39) || (t2 == 45)) {
              ss = "_" + s2
            }
            else {
              ss = s1.toUpperCase() + s2
            }
          }
          name = ss
          //////////////////////////////////////////////////////
          /// ' ' (32) No 2 space allowed next to eachother  ///
          /// ' (39)  No 2 ' allowed next to eachother       ///
          /// - (45)   No 2 - allowed next to eachother      ///
          /// they cant be at the beginning                  ///
          /// all combos are also not allowed                ///
          //////////////////////////////////////////////////////
          name = name.replace("  ", " ");
          name = name.replace(" -", "-");
          name = name.replace(" '", "'");
          name = name.replace("- ", "-");
          name = name.replace("' ", "'");
          name = name.replace("''", "'");
          name = name.replace("--", "-");
          name = name.replace("-'", "-");
          name = name.replace("'-", "'");
          /////////////////////////////////////////////////////////////////////
          ///  check the words: xx_xx_xx is allowed / xxx_x is not allowed  ///
          ///  x'xx is allowed / xx_x'xx is allowed x                       ///
          ///  a hyphen can be considered as space                          ///
          /////////////////////////////////////////////////////////////////////
          if (name.length < 5) {
            err1 = true
          }
          else {
            err1 = false
            ss = ""
            for (i = 0; i < name.length; i++) {
              s1 = name.substring(i, i + 1) //is the last char of the on capital tested assemblystring
              t1 = s1.charCodeAt(0);      //is the code of the on capital tested char
              if ((t1 == 32) || (t1 == 39) || (t1 == 45)) {
                if (t1 == 39) {
                  ss = ss + "'"
                }
                else {
                  ss = ss + " "
                }
              }
              else {
                ss = ss + "X"
              }
            }
            /////////////////////////////////////////////////
            ///  first all the wrong quote possibilities  ///
            /////////////////////////////////////////////////
            //if (name.length == 7) {alert(ss)}
            if (ss.indexOf("'X ") > -1) { err1 = true }
            if (ss.indexOf("'X'") > -1) { err1 = true }
            if (ss.indexOf("XX'") > -1) { err1 = true }
            //////////////////////////////////////
            ///  then the space possibilities  ///
            //////////////////////////////////////
            if (ss.indexOf(" X ") > -1) { err1 = true }
            if (ss.indexOf("XX XX") < 0) { err1 = true }
            if (ss.substring(0, 2) == "X ") { err1 = true }
            if (ss.substring(ss.length - 2, ss.length) == " X") { err1 = true }
            //if (name.length == 7) {alert(err1)}
            //////////////////////////////////////
          }
        }
      }
      else {  //name.length = 1
        name = name.toUpperCase()
        err1 = true //min 2 words
        err2 = true //between 5 and 32 char
        err3 = false //no _ characters
        if (name == "_") {
          err3 = true //there ia a _ character
        }
      }
    } else {  //name.length = 0
      err1 = true //min 2 words
      err2 = true //between 5 and 32 char
      err3 = false //no _ characters
    }
  } else { // name.length = 0
    err1 = true //min 2 words
    err2 = true //between 5 and 32 char
    err3 = false //no _ characters
  }
  tt = name.length
  document.getElementById("mySpan2").innerHTML = "between 5 and 32 characters: [" + String(tt) + "]"
  document.getElementById("name").value = name
  document.getElementById("mySpan1").style.color = "green"
  document.getElementById("mySpan2").style.color = "green"
  document.getElementById("mySpan3").innerHTML = ""
  if (err1) {
    document.getElementById("mySpan1").style.color = "red"
  }
  if (err2) {
    document.getElementById("mySpan2").style.color = "red"
  }
  if (err3) {
    document.getElementById("mySpan3").innerHTML = "Please remove all ' _' characters."
  }

  if ((err1) || (err2) || (err3)) {
    document.getElementById("nameContinue").disabled = true;
    document.getElementById("nameContinue").classList.add("disabled_button");
  } else {
    document.getElementById("nameContinue").disabled = false;
    document.getElementById("nameContinue").classList.remove("disabled_button");
  }
}


function nameEnter() {
  var name = document.getElementById("name").value
  window.localStorage.setItem('myName', name);
  window.location.href = "400_idcard.html";
}