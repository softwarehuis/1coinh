
function OnStart() {
  var gender = window.localStorage.getItem('myGender')
  if (gender == null) { gender = "" }
  if (gender == "Male") {
    document.getElementById("Male").checked = true
  }
  if (gender == "Female") {
    document.getElementById("Female").checked = true
  }
  if (gender == "Other") {
    document.getElementById("Other").checked = true
  }
  genderSelect()
}


function genderSelect() {
  var gender = ""
  if (document.getElementById("Male").checked) {
    gender = "Male"
  }
  if (document.getElementById("Female").checked) {
    gender = "Female"
  }
  if (document.getElementById("Other").checked) {
    gender = "Other"
  }
  if (gender == "") {
    document.getElementById("genderContinue").disabled = true;
    document.getElementById("genderContinue").classList.add("disabled_button");
  }
  else {
    document.getElementById("genderContinue").disabled = false;
    document.getElementById("genderContinue").classList.remove("disabled_button");
  }
}


function genderEnter() {
  var gender = ""
  if (document.getElementById("Male").checked) {
    gender = "Male"
  }
  if (document.getElementById("Female").checked) {
    gender = "Female"
  }
  if (document.getElementById("Other").checked) {
    gender = "Other"
  }
  window.localStorage.setItem('myGender', gender);
  window.location.href = "400_idcard.html";
}