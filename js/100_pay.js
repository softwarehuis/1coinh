
var fileContent = ""
var myBlockchainTXT = ""

function OnStart()
{
    document.getElementById("loadtext").addEventListener('change', handleFile, false);
    //////////////////////////////
    ///  read your blockchain  ///
    //////////////////////////////
    var myFiles = ""  //to get the filename of your blockchain
    var read = "" //to read your blockchain
    var s1 = window.localStorage.getItem('myFiles')
    if (s1 == null) {s1 = ""}
    if ((s1 != s1) || (s1 == "")) //catch a NaN issue
    {
        read = ""
        alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.")
    }
    else
    {
        var myBlockchain = s1 + ".txt"
        var s1 = window.localStorage.getItem(myBlockchain)
        if (s1 == null) {s1 = ""}
        if ((s1 != s1) || (s1 == "")) //catch a NaN issue
        {
            myBlockchainTXT = ""
            alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.")
        }
        else
        {
            myBlockchainTXT = s1;
        }
    }
}



function handleFile(e) 
{
    var fileSize = 0;
    var theFile = document.getElementById("loadtext");
    if (theFile.value.indexOf("proposal.txt") > -1)
    {
        if (typeof (FileReader) != "undefined") 
        {
            var headerLine = "";
            //create html5 file reader object
            var myReader = new FileReader();
            // call filereader. onload function
            myReader.onload = function(e) 
            {
                fileContent = myReader.result;
                processProposal()
            }
            //call file reader onload
            myReader.readAsText(theFile.files[0]);
        }
        else 
        {
            alert("This browser does not support HTML5.");
        }      
    }
    else 
    {
        alert("Please upload a valid proposal.txt file.");
    }
    return false;
}



function loadIt()
{
  document.getElementById("loadtext").click();
}


function processProposal()
{
    var s1 = fileContent
    var t0 = s1.indexOf("<")
    var ss = s1.substring(0,t0) // proposal part
    s1 = s1.substring(t0+1)
    t0 = s1.indexOf("~")
    var myBlockchain = s1.substring(0,t0) // blockchain part

    var s2 = myBlockchain.substring(0,64)
    var s3 = myBlockchainTXT.substring(0,64)
    if (s2 == s3)
    {
        alert("You can't read your own proposal.txt file. This file can only be used by others!")
        window.open("index.html","_self");
    }
    else
    {
        var myIDCard = s1.substring(t0+1)
        window.localStorage.setItem("proposal",ss)
        /////////////////////////////////////////////////////////////
        ///  find the hash of the person between 3rd and 4th "|"  ///
        /////////////////////////////////////////////////////////////
        s1 = ss;                     //s1 = 0|0.5|Teun van Sambeek|2ad9e8e0d283875e503207397116e48c00a6e249c909eec3ae657e690ef0ede4|>4698:3842610<
        var t0 = s1.indexOf("|")
        var s1 = s1.substring(t0+1)  //s1 = 0.5|Teun van Sambeek|2ad9e8e0d283875e503207397116e48c00a6e249c909eec3ae657e690ef0ede4|>4698:3842610<
        t0 = s1.indexOf("|")
        s1 = s1.substring(t0+1)  //s1 = Teun van Sambeek|2ad9e8e0d283875e503207397116e48c00a6e249c909eec3ae657e690ef0ede4|>4698:3842610<
        t0 = s1.indexOf("|")
        s1 = s1.substring(t0+1,t0+65)  //s1 = 2ad9e8e0d283875e503207397116e48c00a6e249c909eec3ae657e690ef0ede4
        ////////////////////////////////////////////////////////////////////////////////////////////////
        ///  here you could compare possible earlier versions of blockchains you have of the person  ///
        ///  TODO                                                                                    ///
        ////////////////////////////////////////////////////////////////////////////////////////////////
        var fname = s1 + ".txt"
        window.localStorage.setItem(fname,myBlockchain)
        fname = s1 + ".png"
        window.localStorage.setItem(fname,myIDCard)
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //alert("Proposal id uploaded!")
        window.open("102_pay2.html","_self");
    }
}



function downloadPayInfo()
{
    //////////////////////////////
    ///  read your blockchain  ///
    //////////////////////////////
    var myFiles = ""  //to get the filename of your blockchain
    var ss = ""
    var s1 = ""
    var s2 = ""
    s1 = window.localStorage.getItem('myFiles')
    if (s1 == null) {s1 = ""}
    if ((s1 != s1) || (s1 == "")) //catch a NaN issue
    {
        read = ""
        alert("You don't have a personal blockchain. Go to 'IDCard' to set your IDCard.")
    }
    else
    {
        var myBlockchain = s1 + ".txt"
        var myIDCard = s1 + ".png" //base64 code of IDCard
        s1 = window.localStorage.getItem(myBlockchain)
        if (s1 == null) {s1 = ""}
        s2 = window.localStorage.getItem(myIDCard)
        if (s2 == null) {s2 = ""}
        if ((s1 != s1) || (s1 == "") || (s2 != s2) || (s2 == "")) //catch a NaN issue
        {
            alert("You don't have a personal blockchain and/or IDCard. Go to 'IDCard' to create both.")
        }
        else
        {
            /////////////////////////////////////////////////////////////////////////////////////////////////////
            ///  Now we create one file: "payment_id.txt" where both my IDCard and Blockchain info are included  ///
            ///  The size of the IDCards is about 200K. Probably you will do transactions with not more than  ///
            ///  1000 people, which makes all IDCards together a maximum of 200 MB, your Blockchain           ///
            ///  transactions are probably around 1KB per transaction. With 1000 transactions a year, you     ///
            ///  get to about 10MB in 10 years, but the blockchains of others, you collect also increases to  ///
            ///  10MB in 10 year, which ultimately (after 10 years) is 10GB. So a 16GB mini-USB-stick should  ///
            ///  be able to be sufficient to backup your 1CoinH data for the next 15 years                    ///
            /////////////////////////////////////////////////////////////////////////////////////////////////////
            ss = s2 + ">" + s1  // so the order is: IDCARD ">" BLOCKCHAIN                    ///
            ///  if a person has multiple IDCards, they need to be added after this string,  ///
            ///  each one starting with a "%" character  TODO!!!                             ///
            ////////////////////////////////////////////////////////////////////////////////////
            var element = document.createElement('a');
            element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(ss));
            element.setAttribute('download', "payment_id.txt");
            element.style.display = 'none';
            document.body.appendChild(element);
            element.click();
            document.body.removeChild(element);
        }
    }
}

