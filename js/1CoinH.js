

function OnStart()
{
  if (!lsTest()) 
  {
    alert("ERROR!  This browser doesn't support 'LocalStorage' and is therefore not capable to run the 1CoinH app. Try installing another Internet-Browser on your system.")
  }
}


function lsTest() 
{
  var test = 'test';
  try 
  {
    localStorage.setItem(test, test);
    localStorage.removeItem(test);
    return true;
  } 
  catch (e) 
  {
    return false;
  }
}