
function OnStart() {
  var eye = window.localStorage.getItem('myLeftEye')
  if (eye == null) { eye = "" }
  if (eye == "Prosthesis") { document.getElementById("prosthesis").checked = true }
  if (eye == "Amber") { document.getElementById("amber").checked = true }
  if (eye == "Blue") { document.getElementById("blue").checked = true }
  if (eye == "Brown") { document.getElementById("brown").checked = true }
  if (eye == "Grey") { document.getElementById("grey").checked = true }
  if (eye == "Green") { document.getElementById("green").checked = true }
  if (eye == "Hazel") { document.getElementById("hazel").checked = true }
  if (eye == "Red") { document.getElementById("red").checked = true }
  if (eye == "Blue-Grey") { document.getElementById("bluegrey").checked = true }
  if (eye == "Blue-Green") { document.getElementById("bluegreen").checked = true }
  if (eye == "Green-Grey") { document.getElementById("greengrey").checked = true }

  eyeSelect()
}


function eyeSelect() {
  var eye = ""
  if (document.getElementById("prosthesis").checked) { eye = "Prosthesis" }
  if (document.getElementById("amber").checked) { eye = "Amber" }
  if (document.getElementById("blue").checked) { eye = "Blue" }
  if (document.getElementById("brown").checked) { eye = "Brown" }
  if (document.getElementById("grey").checked) { eye = "Grey" }
  if (document.getElementById("green").checked) { eye = "Green" }
  if (document.getElementById("hazel").checked) { eye = "Hazel" }
  if (document.getElementById("red").checked) { eye = "Red" }
  if (document.getElementById("bluegrey").checked) { eye = "Blue-Grey" }
  if (document.getElementById("bluegreen").checked) { eye = "Blue-Green" }
  if (document.getElementById("greengrey").checked) { eye = "Green-Grey" }

  if (eye == "") {
    document.getElementById("eyeContinue").disabled = true;
    document.getElementById("eyeContinue").classList.add("disabled_button");
  } else {
    document.getElementById("eyeContinue").disabled = false;
    document.getElementById("eyeContinue").classList.remove("disabled_button");
  }
}


function eyeEnter() {
  var eye = ""
  if (document.getElementById("prosthesis").checked) { eye = "Prosthesis" }
  if (document.getElementById("amber").checked) { eye = "Amber" }
  if (document.getElementById("blue").checked) { eye = "Blue" }
  if (document.getElementById("brown").checked) { eye = "Brown" }
  if (document.getElementById("grey").checked) {
    eye = "Grey"
  }
  if (document.getElementById("green").checked) {
    eye = "Green"
  }
  if (document.getElementById("hazel").checked) {
    eye = "Hazel"
  }
  if (document.getElementById("red").checked) {
    eye = "Red"
  }
  if (document.getElementById("bluegrey").checked) {
    eye = "Blue-Grey"
  }
  if (document.getElementById("bluegreen").checked) {
    eye = "Blue-Green"
  }
  if (document.getElementById("greengrey").checked) {
    eye = "Green-Grey"
  }
  window.localStorage.setItem('myLeftEye', eye);
  window.location.href = "400_idcard.html";
}